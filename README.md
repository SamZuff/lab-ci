# SEm-225 I5 CI/TDD lab

## Abstract
This repository is used for "CI/TDD" lab of the S.Em. 225 module at HES-SO Valais-Wallis in Sion.

## Repository Description

### src
This sub directory contains all the C source code for this repository.

### test
This sub directory contains all the automated unit and system tests (using GitLab CI) associated with this repository.

![coverage](https://gitlab.com/SamZuff/lab-ci/badges/master/pipeline.svg?style=flat)

## Getting Started
Fork this repository from the appropriate git source server to have your own working copy. Contribute with your own additions in your own repository.

## Prerequisites
A local installation of git is required.

## Running the tests
There are a number of simple automated (using GitLab CI) tests in this repository.

## Built With

* Git - A distributed version-control system for tracking changes in source code

* GitLab - a web-based DevOps life cycle tool that provides a Git repository manager

* GitLab CI - The Continuous Integration tool suite available in GitLab

* C - A general-purpose, procedural computer programming language

* Unity - A unit test framework for the C programming language (Especially Embedded Software) [http://www.throwtheswitch.org/unity](http://www.throwtheswitch.org/unity)

* Ruby - An interpreted, high-level, general-purpose programming language

* Python - The Python programming language

## Contributing
To contribute, fork this repository first and then add content in your own copy of the repository.

## Versioning
We use [Semantic Versioning](https://semver.org/). For the list of versions available for this repo, see the tags on this repository.

## Authors
* Steve Devenes
* Jerome Corre
* Christopher Métrailler

## License
Copyright (C) 2020 HES-SO Valais-Wallis - All Rights Reserved
